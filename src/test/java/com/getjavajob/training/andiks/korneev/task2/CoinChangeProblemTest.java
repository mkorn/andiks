package com.getjavajob.training.andiks.korneev.task2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by mkorn on 09.02.2016.
 */
public class CoinChangeProblemTest {

    @Test
    public void countWaysToProduceZeroAmountOfMoneyTest() {
        int actualCount = CoinChangeProblem.countWaysToProduceGivenAmountOfMoney(0);
        int expectedCount = 1;
        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void countWaysToProduceNegativeAmountOfMoneyTest() {
        int actualCount = CoinChangeProblem.countWaysToProduceGivenAmountOfMoney(-1);
        int expectedCount = 0;
        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void countWaysToProduceGivenAmountOfMoneyTest() {
        int actualCount = CoinChangeProblem.countWaysToProduceGivenAmountOfMoney(1);
        int expectedCount = 1;
        assertEquals(expectedCount, actualCount);
        actualCount = CoinChangeProblem.countWaysToProduceGivenAmountOfMoney(11);
        expectedCount = 4;
        assertEquals(expectedCount, actualCount);
    }
}
