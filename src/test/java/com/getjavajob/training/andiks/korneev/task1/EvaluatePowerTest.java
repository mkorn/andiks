package com.getjavajob.training.andiks.korneev.task1;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by mkorn on 09.02.2016.
 */
public class EvaluatePowerTest {

    private static final int ARRAY_SIZE = 5;
    private static final int POWER = 2;

    private static int[] testArray;
    private static int[] expectedArray = {0, 1, 4, 9, 16};

    @BeforeClass
    public static void init() {
        testArray = initArray(ARRAY_SIZE);
    }

    @Test
    public void singleEvaluateTest() {
        int[] actualArray = EvaluatePower.evaluate(testArray, POWER);
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void multiEvaluateTest() throws Exception {
        int[] actualArray = EvaluatePower.multiEvaluate(testArray, POWER);
        assertArrayEquals(expectedArray, actualArray);
    }

    private static int[] initArray(int n) {
        int[] testArray = new int[n];
        for (int i = 0; i < n; i++) {
            testArray[i] = i;
        }
        return testArray;
    }
}
