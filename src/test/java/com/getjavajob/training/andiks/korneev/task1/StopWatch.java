package com.getjavajob.training.andiks.korneev.task1;

public class StopWatch {
    private long startTime;

    public long start() {
        startTime = System.currentTimeMillis();
        return startTime;
    }

    public long getElapsedTime() {
        long finishTime = System.currentTimeMillis();
        return finishTime - startTime;
    }
}
