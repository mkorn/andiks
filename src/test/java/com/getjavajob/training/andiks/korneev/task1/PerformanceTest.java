package com.getjavajob.training.andiks.korneev.task1;

/**
 * Created by mkorn on 09.02.2016.
 */
public class PerformanceTest {

    public static final int ARRAY_SIZE = 100_000;
    public static final int POWER = 5;

    public static void main(String[] args) throws Exception {
        //singleEvaluate();
        multiEvaluate();
    }

    /*
    * Performance test for single thread realization
    */
    public static void singleEvaluate() {
        StopWatch stopWatch = new StopWatch();
        int[] testArray = initArray(ARRAY_SIZE);
        stopWatch.start();
        EvaluatePower.evaluate(testArray, POWER);
        long elapsedTime = stopWatch.getElapsedTime();
        System.out.println("Elapsed time: " + elapsedTime + " ms");
    }

    /*
    * Performance test for multithreading realization
    */
    public static void multiEvaluate() throws Exception {
        StopWatch stopWatch = new StopWatch();
        int[] testArray = initArray(ARRAY_SIZE);
        stopWatch.start();
        EvaluatePower.multiEvaluate(testArray, POWER);
        long elapsedTime = stopWatch.getElapsedTime();
        System.out.println("Elapsed time: " + elapsedTime + " ms");
    }

    private static int[] initArray(int n) {
        int[] testArray = new int[n];
        for (int i = 0; i < n; i++) {
            testArray[i] = i;
        }
        return testArray;
    }
}
