package com.getjavajob.training.andiks.korneev.task1;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mkorn on 09.02.2016.
 */
public class EvaluatePower {

    private static final int THRESHOLD = 10_000;

    /*
     * Single thread realization
    */
    public static int[] evaluate(int[] data, int p) {
        int[] resultArray = new int[data.length];
        for (int i = 0; i < data.length; i++) {
            int current = data[i];
            int result = ExtLib.eval(current, p);
            resultArray[i] = result;
        }
        return resultArray;
    }

    /*
    * Multithreading realization
    */
    public static int[] multiEvaluate(int[] data, int p) throws InterruptedException {
        int[] resultArray = new int[data.length];
        int poolSize = data.length <= THRESHOLD ? data.length : THRESHOLD;
        ExecutorService cachedThreadPool = Executors.newFixedThreadPool(poolSize);
        CountDownLatch latch = new CountDownLatch(data.length);
        for (int i = 0; i < data.length; i++) {
            cachedThreadPool.execute(new RunnablePower(data[i], p, resultArray, i, latch));
        }
        latch.await();
        cachedThreadPool.shutdown();
        return resultArray;
    }
}
