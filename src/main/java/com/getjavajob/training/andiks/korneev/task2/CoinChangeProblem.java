package com.getjavajob.training.andiks.korneev.task2;

/**
 * Created by mkorn on 09.02.2016.
 */
public class CoinChangeProblem {

    public static int countWaysToProduceGivenAmountOfMoney(int cents) {
        int[] s = {1, 5, 10, 25, 50};
        int m = s.length;
        return count(s, m, cents);
    }

    /*
    * Returns the count of ways we can sum  S[0...m-1] coins to get sum n
    * */
    private static int count(int[] s, int m, int n) {
        // If n is 0 then there is 1 solution (do not include any coin)
        if (n == 0) {
            return 1;
        }
        // If n is less than 0 then no solution exists
        if (n < 0) {
            return 0;
        }
        // If there are no coins and n is greater than 0, then no solution exist
        if (m <= 0 && n >= 1) {
            return 0;
        }
        // Count is sum of solutions 1) including S[m-1] 2) excluding S[m-1]
        return count(s, m - 1, n) + count(s, m, n - s[m - 1]);
    }
}
