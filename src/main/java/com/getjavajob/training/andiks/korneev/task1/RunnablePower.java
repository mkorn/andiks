package com.getjavajob.training.andiks.korneev.task1;

import java.util.concurrent.CountDownLatch;

/**
 * Created by mkorn on 09.02.2016.
 */
public class RunnablePower implements Runnable {

    private int number;
    private int power;
    private int[] resultArray;
    private int idx;
    private CountDownLatch latch;

    public RunnablePower(int number, int power, int[] resultArray, int idx, CountDownLatch latch) {
        this.number = number;
        this.power = power;
        this.resultArray = resultArray;
        this.idx = idx;
        this.latch = latch;
    }

    @Override
    public void run() {
        resultArray[idx] = ExtLib.eval(number, power);
        latch.countDown();
    }
}
