package com.getjavajob.training.andiks.korneev.task1;

/**
 * Created by mkorn on 09.02.2016.
 */
public class ExtLib {

    public static int eval(int number, int power) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (power == 0) {
            number = 1;
        } else {
            int currentValue = number;
            for (int i = 1; i < power; i++) {
                number *= currentValue;
            }
        }
        return number;
    }
}
